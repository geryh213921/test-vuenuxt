export const getters = {
    items(state){
        return state.items;
    }
}

export const state = () => ({
    items: []
})


export const mutations = {
    SET_ALL (state, posts) {
        state.items = posts;
    }
}

export const actions = {
    setAll ({ commit }, user) {
        fetch('https://jsonplaceholder.typicode.com/posts')
            .then(response => response.json())
            .then(json => {
                if(json.length > 0){
                    commit('SET_ALL', json);
                }
            })
    }
}