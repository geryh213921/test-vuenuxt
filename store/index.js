export const getters = {
    isAuthenticated(state) {
        return state.auth.loggedIn
    },

    loggedInUser(state) {
        return state.auth.user
    }
}

export const state = () => ({
    auth: {
        loggedIn: false,
        user: null
    }
})

export const mutations = {
    LOGIN (state, user) {
        state.auth.loggedIn = true;
        state.auth.user = user;
    },
    LOGOUT (state) {
        state.auth.loggedIn = false;
        state.auth.user = null;
    }
}

export const actions = {
    login ({ commit }, user) {
        if(user.username && user.password){
            commit('LOGIN', user);
        }
    },
    logout ({ commit }) {
        commit('LOGOUT');
    }
}